#!/usr/bin/python

import sys
import optparse
import numpy as np
import matplotlib as mpl

mpl.use( 'Agg' )
import matplotlib.pyplot as plt

parser = optparse.OptionParser( )

parser.add_option( '--start_percent', dest = 'start_percet', default = 50 )
parser.add_option( '--input_file', dest = 'input_file', default = '' )

data = np.genfromtxt( 'jitter2.dat', dtype = [ ( 'percent', 'f8' ), \
                                               ( 'gmond_off', 'f8' ), \
                                               ( 'gmond_on', 'f8' ), \
                                               ( 'diff', 'f8' ) ], \
                                     comments = '#' )

percent = data['percent']
gmond_off_data   = data['gmond_off']
gmond_off_min    = np.amin( gmond_off_data )
gmond_off_max    = np.amax( gmond_off_data )
gmond_off_median = np.median( gmond_off_data )
gmond_off_mean   = np.mean( gmond_off_data )
gmond_off_stddev = np.std( gmond_off_data )

gmond_on_data   = data['gmond_on']
gmond_on_min    = np.amin( gmond_on_data )
gmond_on_max    = np.amax( gmond_on_data )
gmond_on_median = np.median( gmond_on_data )
gmond_on_mean   = np.mean( gmond_on_data )
gmond_on_stddev = np.std( gmond_on_data )

diff_data   = data['diff']
diff_min    = np.amin( diff_data )
diff_max    = np.amax( diff_data )
diff_median = np.median( diff_data )
diff_mean   = np.mean( diff_data )
diff_stddev = np.std( diff_data )

start = 14
xrange = np.arange( start, percent.size )
labels = [ str( x ) for x in percent[start:] ]

fig, plt1 = plt.subplots( )

l1, = plt1.plot( xrange, gmond_off_data[start:], '+-', label = 'off', color = 'blue' )
l2, = plt1.plot( xrange, gmond_on_data[start:], 'x-', label = 'on', color = 'green' )
plt1.set_ylabel( 'nanosecond' )
# plt1.legend( loc = 2 )
plt1.grid( True )
plt1.set_xlabel( 'percentage' )

plt2 = plt1.twinx( )
l3, = plt2.plot( xrange, diff_data[start:], '-', label = 'diff', color = 'red' )
plt2.set_ylabel( 'relative diff' )
# plt2.legend( loc = 6 )
plt2.grid( True )

for t2 in plt2.get_yticklabels( ):
    t2.set_color( 'red' )

plt.legend( ( l1, l2, l3 ), ( 'off', 'on', 'diff' ), loc = 'upper left' )
plt.axis( xmin = start, xmax = 22 )
plt.xticks( xrange, labels )
plt.title( 'gmond jitter' )
plt.savefig( 'gmond_jitter.png' )
