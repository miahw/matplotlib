#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description='boxplot')

parser.add_argument('--input-file', dest='input_file', type=str,
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str,
                    help='Output image')
parser.add_argument('--title', dest='title', type=str,
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('procs_2', 'i8'), \
                                                 ('procs_4', 'i8'), \
                                                 ('procs_8', 'i8'), \
                                                 ('procs_16', 'i8'), \
                                                 ('procs_32', 'i8'), \
                                                 ('procs_64', 'i8') ], \
                                                 ('procs_128', 'i8'), \
                                                 ('procs_256', 'i8' ) ], \
                      comments = '#' )

procs_2 = data['procs_2'] * 1.04858
procs_4 = data['procs_4'] * 1.04858
procs_8 = data['procs_8'] * 1.04858
procs_16 = data['procs_16'] * 1.04858
procs_32 = data['procs_32'] * 1.04858
procs_64 = data['procs_64'] * 1.04858
# procs_128 = data['procs_128']
# procs_256 = data['procs_256']

fig, plt1 = plt.subplots( )

# print( procs_2 )
# print( procs_4 )
# print( procs_8 )
# print( procs_16 )
# print( procs_32 )
l1 = plt1.boxplot( [ procs_2, procs_4, procs_8, procs_16, procs_32, procs_64 ] )
l2 = plt1.plot( [ 1, 2, 3, 4, 5, 6 ], [ max(procs_2), max(procs_4), max(procs_8), max(procs_16), max(procs_32), max(procs_64) ], linewidth = 1, linestyle = 'dashed' )
plt.ylabel( 'MB/s' )
plt.xlabel( 'Number of processes' )

plt.grid( True )
plt.xticks( [ 1, 2, 3, 4, 5, 6 ], [ '2', '4', '8', '16', '32', '64' ] )
plt.title( args.title )
plt.savefig( args.output_image )

