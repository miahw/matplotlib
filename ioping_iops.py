#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='boxplot')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('size', 'i8'), \
                                                 ('read', 'f8'), \
                                                 ('write', 'f8'), \
                                                ], \
                      comments = '#' )

size = data['size']
read = data['read'] * 1000
write = data['write'] * 1000
xrange = np.arange( 0, size.size )

fig, plt1 = plt.subplots( )

l1 = plt1.plot( read, linewidth = 1, color = 'red', label = 'Read', marker = 'o', markersize = 4 )
l2 = plt1.plot( write, linewidth = 1, color = 'green', label = 'Write', marker = '+', markersize = 4 )

plt.legend( loc = 'upper right' )
plt.ylabel( 'IO Operations Per Second (iops)' )
plt.xlabel( 'IO size (kb)' )

plt.grid( True )
plt.xlim( [ 0, size.size - 1 ] )
plt.xticks( xrange, size )
plt.title( args.title )
plt.savefig( args.output_image )
