#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='app_benchmark')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('compiler', 'a3'), \
                                                 ('untuned', 'i8'), \
                                                 ('tuned', 'i8') ], \
                      comments = '#' )

compiler = data['compiler']
untuned = data['untuned']
tuned = data['tuned']

width = 0.4

xrange = np.arange( 0, compiler.size )

fig, plt1 = plt.subplots( )

l1 = plt1.bar( xrange - width + 0.2, untuned, linewidth = 1, color = 'red', width = width, label = r'Untuned' )
l2 = plt1.bar( xrange + 0.2, tuned, linewidth = 1, width = width, color = 'blue', label = r'Tuned' )

plt.legend( loc = 'upper left' )
plt.ylabel( 'Bandiwdth (MB/s)' )
plt.xlabel( 'Compiler and flag (-march=)' )

plt.grid( True )
plt.xlim( [ 1, 4 ] )
# plt.ylim( [ 0, 20000 ] )
plt.xticks( [ 0, 1, 2, 3 ], [ 'GCC (znver3)', 'ICC (core-avx2)', 'ICX (core-avx2)', 'ACC (znver3)' ] )
plt.title( args.title + r' $a_i = \alpha b_i + c_i$')
plt.axis( xmin = -1.0, xmax = compiler.size )
plt.savefig( args.output_image )
