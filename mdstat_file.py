#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='npb')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('processes', 'i8'), \
                                                 ('file_creation', 'f8'), \
                                                 ('file_stat', 'f8'), \
                                                 ('file_read', 'f8') ], \
                      comments = '#' )

processes      = data['processes']
file_creation  = data['file_creation']
file_stat      = data['file_stat']
file_read      = data['file_read']

xrange = np.arange( 0, file_creation.size )

fig1, plt1 = plt.subplots( )

l1 = plt1.plot( xrange, file_creation, linewidth = 1, marker = 'o', markersize = 4, label = 'File creation' )
l2 = plt1.plot( xrange, file_stat, linewidth = 1, markersize = 4, marker = 'o', label = 'File stat' )
l3 = plt1.plot( xrange, file_read, linewidth = 1, markersize = 4, marker = 'v', label = 'File read' )

plt.legend( loc = 'upper left' )
plt.ylabel( 'Operations/sec' )
plt.xlabel( 'Number of processes' )
# plt.yscale('log')

plt.grid( True )
plt.xlim( [ 0, 6 ] )
plt.xticks( xrange, processes )
plt.title( args.title )
plt.savefig( args.output_image )
