#!/usr/bin/python

import os
import re
import sys
import optparse
import numpy as np
import matplotlib as mpl

mpl.use( 'Agg' )
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams.update( { 'figure.autolayout' : True } )

parser = optparse.OptionParser( )

parser.add_option( '--start_percent', dest = 'start_percent', default = 50, \
                   help = 'Start x value, e.g. --start_percent=50' )
parser.add_option( '--input_files', dest = 'input_files', default = '', \
                   help = 'Input file names (comma separated with no spaces), e.g. --input_files=data1.txt,data2.txt' )
parser.add_option( '--image_format', dest = 'image_format', default = 'png', \
                    help = 'Image format. Supported formats: emf, eps, pdf, pgf, png, ps, raw, rgba, svg, svgz. Default is PNG' )
parser.add_option( '--image_file', dest = 'image_file', default = 'output_image.png', \
                   help = 'Output image format.' )
parser.add_option( '--x-label', dest = 'x_label', default = 'Distribution', help = 'x label' )
parser.add_option( '--title', dest = 'title', default = 'benchmark', help = 'Title of the graph' )

options, remainder = parser.parse_args( )

file_list = options.input_files.split( ',' )

jitter_data = []
jitter_min = []
jitter_max = []
jitter_median = []
jitter_mean = []
jitter_stddev = []

i = 0 # iterate through all the files
for file in file_list:
    data = np.genfromtxt( file, dtype = [ ( 'percent', 'f8' ), \
                                          ( 'jitter', 'f8' ) ], \
                                   comments = '#' )

    percent = data['percent']

    # if file lengths are difference size, then exit
    if i > 0 and file_length != percent.size:
      print 'Error: file lengths must be of same size!'
      sys.exit( 1 )

    file_length = percent.size

    jitter_data.append( data['jitter'] )
    jitter_min.append( np.amin( jitter_data[i] ))
    jitter_max.append( np.amax( jitter_data[i] ))
    jitter_median.append( np.median( jitter_data[i] ))
    jitter_mean.append( np.mean( jitter_data[i] ))
    jitter_stddev.append( np.std( jitter_data[i] ))

    # print the statistical values on stdout
    print 'min', file, jitter_min[i]
    print 'max', file, jitter_max[i]
    print 'median', file, jitter_median[i]
    print 'mean', file, jitter_mean[i]
    print 'stddev', file, jitter_stddev[i]
    print
    i += 1

# convert the input argument to 65 bit double precision
start = np.float64( options.start_percent )
# get the array position where the start percent exists
start = np.where( percent == start )[0][0]

# x values start from 0, ..., n but this is re-labelled to percent
xrange = np.arange( start, percent.size )
labels = [ str( x ) for x in percent[start:] ]

# create a subplot. This is generic if a second y axis is required
fig, plt1 = plt.subplots( figsize = ( 10 if start < 10 else 8, 4 ))

i = 0
legends = ( )
legend_labels = ( )

# iterate through the files to create individual plots
for file in file_list:
    first = os.path.splitext( file )[0] # the label is the just the file name without the extension
    label, = plt1.plot( xrange, jitter_data[i][start:], label = first )
    legends = legends + ( label, )
    legend_labels = legend_labels + ( first, )
    i += 1

plt1.set_yscale( 'log' ) # set the y-axis to logscale
plt1.set_ylabel( 'nanosecond', fontsize = 10 )
plt1.grid( True )
plt1.set_xlabel( options.x_label )

for t1 in plt1.get_yticklabels( ):
   t1.set_color( 'black' )
   t1.set_fontsize( 10 )

# set the plots' legends
plt.legend( legends , legend_labels, loc = 'upper left', fontsize = 10 ) 

plt.axis( xmin = start, xmax = 22 )
# change the xtics
plt.xticks( xrange, labels )

for xt in plt1.get_xticklabels( ):
  xt.set_color( 'black' )
  xt.set_fontsize( 10 )
# rotate the xtics by 45 degrees
  xt.set_rotation( 45 )

plt.title( options.title, fontsize = 10 )
# and finally save the image file
plt.savefig( options.image_file, format = options.image_format )
print 'Saved image file', options.image_file
