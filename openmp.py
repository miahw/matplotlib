#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='npb')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('threads', 'i8'), \
                                                 ('aocc_parallel', 'f8'), \
                                                 ('aocc_for', 'f8'), \
                                                 ('aocc_barrier', 'f8'), \
                                                 ('aocc_single', 'f8'), \
                                                 ('aocc_critical', 'f8'), \
                                                 ('aocc_atomic', 'f8'), \
                                                 ('aocc_reduction', 'f8'), \
                                                 ('intel_parallel', 'f8'), \
                                                 ('intel_for', 'f8'), \
                                                 ('intel_barrier', 'f8'), \
                                                 ('intel_single', 'f8'), \
                                                 ('intel_critical', 'f8'), \
                                                 ('intel_atomic', 'f8'), \
                                                 ('intel_reduction', 'f8') ], \
                      comments = '#' )

threads         = data['threads']

aocc_parallel   = data['aocc_parallel']
aocc_for        = data['aocc_for']
aocc_barrier    = data['aocc_barrier']
aocc_single     = data['aocc_single']
aocc_critical   = data['aocc_critical']
aocc_atomic     = data['aocc_atomic']
aocc_reduction  = data['aocc_reduction']

intel_parallel   = data['intel_parallel']
intel_for        = data['intel_for']
intel_barrier    = data['intel_barrier']
intel_single     = data['intel_single']
intel_critical   = data['intel_critical']
intel_atomic     = data['intel_atomic']
intel_reduction  = data['intel_reduction']

width = 0.2

xrange = np.arange( 0, aocc_parallel.size )

fig1, plt1 = plt.subplots( )

l1 = plt1.plot( xrange, aocc_parallel, linewidth = 1, marker = 'o', label = r'AOCC PARALLEL' )
l2 = plt1.plot( xrange, intel_parallel, linewidth = 1, marker = 'o',label = r'Intel PARALLEL' )
l3 = plt1.plot( xrange, aocc_for, linewidth = 1, marker = 'v', label = r'AOCC FOR' )
l4 = plt1.plot( xrange, intel_for, linewidth = 1, marker = 'v', label = r'Intel FOR' )
l5 = plt1.plot( xrange, aocc_barrier, linewidth = 1, marker = '.', label = r'AOCC BARRIER' )
l6 = plt1.plot( xrange, intel_barrier, linewidth = 1, marker = '.', label = r'Intel BARRIER' )
l7 = plt1.plot( xrange, aocc_single, linewidth = 1, marker = ',', label = r'AOCC SINGLE' )
l8 = plt1.plot( xrange, intel_single, linewidth = 1, marker = ',', label = r'Intel SINGLE' )

plt.legend( loc = 'upper left' )
plt.ylabel( r'Overheaad $\mu s$' )
plt.xlabel( 'Number of threads' )
# plt.yscale('log')

plt.grid( True )
plt.xlim( [ 0, 6 ] )
plt.xticks( xrange, threads )
plt.title( args.title )
# plt.axis( xmin = -0.5, xmax = aocc_parallel.size - 2* width )
plt.savefig( args.output_image )
