#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='app_benchmark')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('num_nodes', 'i8'), \
                                                 ('intelmpi_untuned', 'f8'), \
                                                 ('intelmpi_tuned', 'f8') ], \
                      comments = '#' )

num_nodes = data['num_nodes'] 
intelmpi_untuned = data['intelmpi_untuned'] 
intelmpi_tuned = data['intelmpi_tuned'] 

width = 0.4

xrange = np.arange( 0, num_nodes.size )

fig, plt1 = plt.subplots( )

l1 = plt1.bar( xrange - width + 0.2, intelmpi_untuned, linewidth = 1, color = 'red', width = width, label = r'Intel MPI Untuned' )
l2 = plt1.bar( xrange + 0.2, intelmpi_tuned, linewidth = 1, width = width, color = 'blue', label = r'Intel MPI Tuned (\texttt{I_MPI_TUNING_MODE=auto})' )

plt.legend( loc = 'upper left' )
plt.ylabel( 'Time (seconds)' )
plt.xlabel( 'Number of nodes (40 CPU cores per node)' )

plt.grid( True )
plt.xlim( [ 1, 4 ] )
plt.ylim( [ 0, 20000 ] )
plt.xticks( [ 0, 1, 2 ], [ '4', '8', '16' ] )
plt.title( args.title )
plt.axis( xmin = -1.0, xmax = num_nodes.size )
plt.savefig( args.output_image )
