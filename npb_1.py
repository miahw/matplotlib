#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='app_benchmark')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('kernel', 'a3'), \
                                                 ('untuned', 'f8'), \
                                                 ('tuned', 'f8') ], \
                      comments = '#' )

kernel = data['kernel']
untuned = data['untuned']
tuned = data['tuned']

width = 0.4

xrange = np.arange( 0, kernel.size )

fig, plt1 = plt.subplots( )

l1 = plt1.bar( xrange - width + 0.2, untuned, linewidth = 1, color = 'red', width = width, label = r'Untuned' )
l2 = plt1.bar( xrange + 0.2, tuned, linewidth = 1, width = width, color = 'blue', label = r'Tuned' )

plt.legend( loc = 'upper left' )
plt.ylabel( 'MFLOP/s (double precision)' )
plt.xlabel( 'Kernel' )

plt.grid( True )
plt.xlim( [ 1, 3 ] )
# plt.ylim( [ 0, 20000 ] )
plt.xticks( [ 0, 1, 2 ], [ 'CG', 'BT', 'MG' ] )
plt.title( args.title )
plt.axis( xmin = -1.0, xmax = kernel.size )
plt.savefig( args.output_image )
