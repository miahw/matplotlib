#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='npb')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('processes', 'i8'), \
                                                 ('directory_creation', 'f8'), \
                                                 ('directory_stat', 'f8'), \
                                                 ('directory_removal', 'f8') ], \
                      comments = '#' )

processes           = data['processes']
directory_creation  = data['directory_creation']
directory_stat      = data['directory_stat']
directory_removal   = data['directory_removal']

xrange = np.arange( 0, directory_creation.size )

fig1, plt1 = plt.subplots( )

l1 = plt1.plot( xrange, directory_creation, linewidth = 1, marker = 'o', markersize = 4, label = 'Directory creation' )
l2 = plt1.plot( xrange, directory_stat, linewidth = 1, markersize = 4, marker = 'o', label = 'Directory stat' )
l3 = plt1.plot( xrange, directory_removal, linewidth = 1, markersize = 4, marker = 'v', label = 'Directory removal' )

plt.legend( loc = 'upper left' )
plt.ylabel( 'Operations/sec' )
plt.xlabel( 'Number of processes' )
# plt.yscale('log')

plt.grid( True )
plt.xlim( [ 0, 6 ] )
plt.xticks( xrange, processes )
plt.title( args.title )
# plt.axis( xmin = -0.5, xmax = aocc_parallel.size - 2* width )
plt.savefig( args.output_image )
