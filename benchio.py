#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='boxplot')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('mpi_io', 'f8'), \
                                                 ('hdf5', 'f8'), \
                                                 ('netcdf', 'f8'), \
                                                 ('mpi_io_hints', 'f8'), \
                                                 ('hdf5_hints', 'f8'), \
                                                 ('netcdf_hints', 'f8'), \
                                                ], \
                      comments = '#' )

mpi_io = data['mpi_io'] * 1.04858
hdf5 = data['hdf5'] * 1.04858
netcdf = data['netcdf'] * 1.04858
mpi_io_hints = data['mpi_io_hints'] * 1.04858
hdf5_hints = data['hdf5_hints'] * 1.04858
netcdf_hints = data['netcdf_hints'] * 1.04858


xrange = np.arange( 1, mpi_io.size )

fig, plt1 = plt.subplots( )
# print(xrange)
# print(mpi_io)
l1 = plt1.plot( mpi_io, linewidth = 1, color = 'red', label = 'MPI-IO', marker = 'o', markersize = 4 )
l2 = plt1.plot( hdf5, linewidth = 1, color = 'green', label = 'HDF5', marker = '+', markersize = 4 )
l3 = plt1.plot( netcdf, linewidth = 1, color = 'blue', label = 'NetCDF', marker = '*', markersize = 4 )

l4 = plt1.plot( mpi_io_hints, linewidth = 1, color = 'red', label = r'MPI-IO \texttt{IBM\_largeblock\_io=true}', marker = 'o', linestyle = '--', markersize = 4 )
l5 = plt1.plot( hdf5_hints, linewidth = 1, color = 'green', label = r'HDF5 \texttt{IBM\_largeblock\_io=true}', marker = '+', linestyle = '--', markersize = 4 )
l6 = plt1.plot( netcdf_hints, linewidth = 1, color = 'blue', label = r'NetCDF \texttt{IBM\_largeblock\_io=true}', marker = '*', linestyle = '--', markersize = 4 )

plt.legend( loc = 'upper left' )
plt.ylabel( 'MB/s' )
plt.xlabel( 'Number of processes' )

plt.grid( True )
plt.xlim( [ 0, 7 ] )
plt.xticks( [ 0, 1, 2, 3, 4, 5, 6, 7 ], [ '2', '4', '8', '16', '32', '64', '128', '256' ] )
plt.title( args.title )
plt.savefig( args.output_image )
