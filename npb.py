#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='npb')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('aocc_bt', 'f8'), \
                                                 ('aocc_cg', 'f8'), \
                                                 ('intel_bt', 'f8'), \
                                                 ('intel_cg', 'f8') ], \
                      comments = '#' )

aocc_bt = data['aocc_bt'] / 1000.0
aocc_cg = data['aocc_cg'] / 1000.0
intel_bt = data['intel_bt'] / 1000.0
intel_cg = data['intel_cg'] / 1000.0
width = 0.2

xrange = np.arange( 0, aocc_bt.size )

fig, plt1 = plt.subplots( )

l1 = plt1.bar( xrange - width - 0.1, aocc_bt, linewidth = 1, color = 'red', width = width, label = r'AOCC \bf{BT}' )
l2 = plt1.bar( xrange - 0.1, intel_bt , linewidth = 1, width = width, color = 'blue', label = r'Intel \bf{BT}' )
l3 = plt1.bar( xrange + width - 0.1, aocc_cg, linewidth = 1, width = width, color = 'green', label = r'AOCC \bf{CG}' )
l4 = plt1.bar( xrange + 2*width - 0.1, intel_cg, linewidth = 1, width = width, color = 'orange', label = r'Intel \bf{CG}' )

plt.legend( loc = 'upper left' )
plt.ylabel( 'GFLOP/s (dp)' )
plt.xlabel( 'Number of threads' )

plt.grid( True )
plt.xlim( [ -1, 8 ] )
plt.xticks( [ 0, 1, 2, 3, 4, 5, 6, 7 ], [ '1', '2', '4', '8', '16', '32', '64', '128' ] )
plt.title( args.title )
plt.axis( xmin = -0.5, xmax = aocc_bt.size - 2* width )
plt.savefig( args.output_image )
