#!/usr/bin/python

import numpy as np
import matplotlib as mpl
mpl.use( 'Agg' )
import matplotlib.pyplot as plt

data = np.genfromtxt( 'npb_cf.dat', dtype = [ ( 'bm_class', 'a4' ), \
                                              ( 'gmond_off', 'f8' ), \
                                              ( 'gmond_on', 'f8' ), \
                                              ( 'diff', 'f8' ) ], \
                                    comments = '#' )
bm_class = data['bm_class']
gmond_off_data   = data['gmond_off']
gmond_off_min    = np.amin( gmond_off_data )
gmond_off_max    = np.amax( gmond_off_data )
gmond_off_median = np.median( gmond_off_data )
gmond_off_mean   = np.mean( gmond_off_data )
gmond_off_stddev = np.std( gmond_off_data )

gmond_on_data   = data['gmond_on']
gmond_on_min    = np.amin( gmond_on_data )
gmond_on_max    = np.amax( gmond_on_data )
gmond_on_median = np.median( gmond_on_data )
gmond_on_mean   = np.mean( gmond_on_data )
gmond_on_stddev = np.std( gmond_on_data )

diff_data   = data['diff']
diff_min    = np.amin( diff_data )
diff_max    = np.amax( diff_data )
diff_median = np.median( diff_data )
diff_mean   = np.mean( diff_data )
diff_stddev = np.std( diff_data )

xrange = np.arange( 0, bm_class.size )
labels = bm_class
width = 0.27

fig, plt1 = plt.subplots( )

l1 = plt1.bar( xrange, gmond_off_data, width = width, label = 'off', color = 'blue', log = True )
l2 = plt1.bar( xrange + width, gmond_on_data, width = width, label = 'on', color = 'green', log = True )
plt1.set_ylabel( 'time (s)' )
plt1.set_ylim( [ 1, 100000 ] )
plt1.grid( True )

plt2 = plt1.twinx( )
l3 = plt2.bar( xrange + 2*width, diff_data, width = width, label = 'diff', color = 'red' )
plt2.set_ylabel( 'relative diff' )
plt2.set_ylim( [ -0.04, 0.04 ] )
plt2.grid( True )

for t2 in plt2.get_yticklabels( ):
  t2.set_color( 'red' )

plt.legend( ( l1, l2, l3 ), ( 'off', 'on', 'diff' ), loc = 'upper right' )
plt.axis( xmin = -0.25, xmax = bm_class.size )
plt.xticks( xrange + width*1.5, bm_class )
plt.title( 'NASA parallel benchmarks with Ganglia - C/Fortran' )
plt.savefig( 'npb_cf.png' )
