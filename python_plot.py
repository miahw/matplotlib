#!/usr/bin/python

import os
import sys
import optparse
import numpy as np
import matplotlib as mpl

mpl.use( 'Agg' )
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams.update( { 'figure.autolayout' : True } )

parser = optparse.OptionParser( )

parser.add_option( '--title', dest = 'title', default = 'Ping pong' )
parser.add_option( '--start-size', dest = 'start_size', default = 0 )
parser.add_option( '--input-file', dest = 'input_file', default = '' )
parser.add_option( '--output-image', dest = 'output_image', default = 'ping_pong.png' )

options, remainder = parser.parse_args( )

if not os.path.exists( options.input_file ):
   raise Exception( 'Input file "' + options.input_file + '" does not exist' )

data = np.genfromtxt( options.input_file, dtype = [ ( 'size', 'i4' ), \
                                                    ( 'direct', 'f8' ), \
                                                    ( 'hostdev', 'f8' ), \
                                                    ( 'hostdev_onload', 'f8' ), \
                                                    ( 'hostdev_onload_latency', 'f8' ), \
                                                    ( 'native', 'f8' ), \
                                                    ( 'native_onload', 'f8' ), \
                                                    ( 'native_onload_latency', 'f8' ) ], \
                                         comments = '#' )

size = data['size']
direct = data['direct']

hostdev = data['hostdev']
hostdev_onload = data['hostdev_onload']
hostdev_onload_latency = data['hostdev_onload_latency']

native = data['native']
native_onload = data['native_onload']
native_onload_latency = data['native_onload_latency']

start = int( options.start_size )
xrange = size
labels = [ str( x ) for x in size[start:] ]

fig, plt1 = plt.subplots( )

l1, = plt1.plot( xrange, direct[start:], label = 'direct bridge (SR-IOV=off)' )
l2, = plt1.plot( xrange, hostdev[start:], label = 'hostdev (SR-IOV=on)' )
l3, = plt1.plot( xrange, hostdev_onload[start:], label = 'hostdev onload (SR-IOV=on)' )
l4, = plt1.plot( xrange, hostdev_onload_latency[start:], label = 'hostdev onload=latency (SR-IOV=on)' )
l5, = plt1.plot( xrange, native[start:], label = 'native (no virt)' )
l6, = plt1.plot( xrange, native_onload[start:], label = 'native onload' )
l7, = plt1.plot( xrange, native_onload_latency[start:], label = 'native onload=latency' )

plt1.set_ylabel( 'time (ns)', fontsize = 10 )
plt1.set_yscale( 'log' )
plt1.grid( True )
plt1.set_xlabel( 'packet size (bytes)', fontsize = 10 )

for t1 in plt1.get_yticklabels( ):
   t1.set_color( 'black' )
   t1.set_fontsize( 10 )

for xt in plt1.get_xticklabels( ):
  xt.set_color( 'black' )
  xt.set_fontsize( 10 )
# rotate the xtics by 45 degrees
  xt.set_rotation( 0 )


plt.legend( ( l1, l2, l3, l4, l5, l6, l7 ), ( 'direct bridge (SR-IOV=off)', \
                                              'hostdev (SR-IOV=on)', 'hostdev onload (SR-IOV=on)', 'hostdev onload=latency (SR-IOV=on)', \
                                              'native (no virt)', 'native onload', 'native onload=latency' ), \
                                              loc = 'upper left', fontsize = 10 )
plt.xscale( 'log', basex = 2 )
# plt.axis( xmin = start, xmax = 22 )
# plt.xticks( xrange, labels )
plt.title( options.title, fontsize = 10 )
plt.savefig( options.output_image )
