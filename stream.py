#!/usr/bin/python

import matplotlib as mpl
import numpy as np 
mpl.use( 'Agg' )
import matplotlib.pyplot as plt
import argparse

mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=r'\usepackage{amsmath}'

parser = argparse.ArgumentParser(description='stream')

parser.add_argument('--input-file', dest='input_file', type=str, \
                    help='Input file')
parser.add_argument('--output-image', dest='output_image', type=str, \
                    help='Output image')
parser.add_argument('--title', dest='title', type=str, \
                    help='Image title')

args = parser.parse_args()

data = np.genfromtxt( args.input_file, dtype = [ ('aocc', 'f8'), \
                                                 ('intel', 'f8') ], \
                      comments = '#' )

aocc = data['aocc'] / 1000.0
intel = data['intel'] / 1000.0

xrange = np.arange( 0, aocc.size )

fig, plt1 = plt.subplots( )
width = 0.2

l1 = plt1.bar( xrange - 0.1, aocc, linewidth = 1, width = width, color = 'red', label = r'\bf{AOCC}' )
l2 = plt1.bar( xrange + width - 0.1, intel, linewidth = 1, width = width, color = 'green', label = r'\bf{Intel}' )

plt.legend( loc = 'upper left' )
plt.ylabel( 'GB/s' )
plt.xlabel( 'Number of threads' )

plt.grid( True )
plt.xlim( [ 0, 7 ] )
plt.xticks( [ 0, 1, 2, 3, 4, 5, 6, 7 ], [ '1', '2', '4', '8', '16', '32', '64', '128' ] )
plt.title( args.title )
plt.axis( xmin = -0.5, xmax = aocc.size - 2* width )
plt.savefig( args.output_image )
